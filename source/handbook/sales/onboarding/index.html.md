---
layout: markdown_page
title: "Sales Onboarding"
---

## **Sales Onboarding Process** 

Sales onboarding at GitLab is focused on what new sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** within their first month or so on the job so they are "customer conversation ready" and they are comfortable, competent, and confident leading discovery calls to begin building pipeline by or before the end of their first several weeks on the job. 

Topics covered include but are not limited to:
* Understanding the GitLab Buyer
* Understanding the Industry in Which We Compete
* Salesforce Basics
* Introduction to GitLab Sales Process & Tools
* Why GitLab and the GitLab Portfolio
* The Competition
* Critical Sales Skills & Behaviors
* GitLab Product Tiers

The process:
*  The GitLab Candidate Experience team initiates a general GitLab onboarding issue for every new GitLab team member
   - See the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
   - In the "Day 1 Accounts and Paperwork" section of the general GitLab onboarding issue, sales managers are instructed to create an Access Request (sales role-based templates are available in the list of template on [this page](https://gitlab.com/gitlab-com/access-requests)
*  In the "Sales Division" section of that issue, Sales Enablement is tagged with the action to add the new sales team member to the Sales Quickstart learning path in Google Classroom. This learning path is designed to accelerate the new sales team member's time to productivity by focusing on the key sales-specific elements they need to know and be able to do within their first several weeks at GitLab.
   - The new sales team member will receive an email prompting them to login to Google Classroom to begin working through the Sales Quickstart learning path  
   - When possible, Sales Enablement will also update the new team member's onboarding issue and/or add a comment to explicitly reference the link to their Sales Quickstart learning path
   - In the "New Team Member" section, there is a specific action for the new hire to "Complete your Sales Quickstart learning path"
   - See what's included in the virtual, self-paced [Sales Quickstart learning path in Google Classroom](/handbook/sales/onboarding/#sales-quickstart-learning-path)
*  Targeted roles for the sales onboarding training issue include Enterprise and Public Sector Strategic Account Leaders (SALs), Mid-Market Account Executives, SMB Customer Advocates, and Inside Sales Reps
   - Customer Success (e.g. Solution Architects, Technical Account Managers, and Professional Services Engineers) and Sales Development Rep (SDR) roles will continue to maintain their own separate onboarding process, but we will be collaborating and sharing best practices
*  In addition to the sales onboarding training issue, new sales team members (along with new Solution Architects, Technical Account Managers, and Sales Development Rep) participate in an interactive 3-day Sales QuickStart workshop full of mock calls, role plays, and group discussions. This format allows new sales team members to practice applying this new knowledge in a fun and challenging way. As a result, new sales team members exit Sales QuickStart more confident and capable in their ability to lead an effective discovery call with a customer or prospect. _(Note: The Results Value is our highest priority and we strive to deliver sales training and enablement remotely as much as possible and reserve in-person formats only for circumstances when we can't obtain the desired results via remote/virtual delivery). Iteration is expected to keep moving what we can to remote models once we can achieve the same or better results that way_.
   - We conduct a hands-on exercise to help new team members get more comfortable navigating issues and submitting Merge Requests (abbreviated to MR) in GitLab 
   - Participants benefit by establishing a community of similar-tenured peers to support each others' growth and development at GitLab
   - We take time to collect feedback on how we can continue to iterate and improve the overall sales onboarding experience
   - See what's included in the [Sales QuickStart 3-day workshop](/handbook/sales/onboarding/#sales-quickstart-workshop)
   - For an overview and feedback summary of Sales QuickStart 1, [click here](https://docs.google.com/presentation/d/1SwbhVZTMgJ9w2jQXWM8go9RxvTrCIBkScCEyhRsaZLA/edit?usp=sharing) 
*  Future iteration of this process will also begin to define what sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** (by sales role, customer segment, and geo as appropriate) after they complete the above and support those learning objectives as they continue their ramp to GitLab sales performance excellence!

## **Sales Quickstart learning path**

### 1.Welcome to GitLab Sales! 
*  **Let's Git To Know Each Other! (assignment, 5 minutes, 1 point)**
   - Please complete this brief [google form](https://docs.google.com/forms/d/e/1FAIpQLScXH3QSAcqUP4mRJsqUWbn7BUJS_SYJVjFg2oXqOoOwzBMzLA/viewform) to introduce yourself.
*  **What is GitLab? (video, 3 minutes, 0 points)**
   - GitLab is a single application for the entire DevOps lifecycle. [Watch the video](https://www.youtube.com/watch?v=MqL6BMOySIQ).
*  **GitLab Culture (video, 3 minutes, 0 points)**
   - Every year, our entire remote workforce gets together in one location for the GitLab Summit. We use this time to bond, build community, and get a bit of work done. It's an essential part of the GitLab experience—watch the video to learn more about our culture, and what it's like to be on our globally distributed team.
   - [Watch the video](https://www.youtube.com/watch?v=Mkw1-Uc7V1k)
   - Read the [Handbook](https://about.gitlab.com/company/culture/gitlab-101/)
*  **Everyone Can Contribute (video, 3 minutes, 0 points)**
   - Learn more about how we live out our Contribute value! [Watch the video](https://www.youtube.com/watch?v=V2Z1h_2gLNU).
*  **Org Chart (Handbook, 0 points)**
   - You can see more information about team members and who reports to whom on the team page. Throughout the course you will be asked to schedule a few brief meetings with your peers. Keep in mind that it is always ok to put a meeting on someone's calendar, if they can't make it and decline it is not a problem. We hope you enjoy getting to know your super cool co-workers!
   - Check out the [org chart](https://about.gitlab.com/company/team/org-chart/) and the [Team Page](https://about.gitlab.com/company/team/)

Note: Details and links to the below will be added soon.

### 2. DevOps Technology Landscape
*  The Software Development Lifecycle
*  The Industry In Which GitLab Competes
*  Review & Subscribe: Industry Insights
*  What is DevOps?
*  Deliver Better Products Faster
*  Increase Operational Efficiencies
*  QUIZ: Increase Operational Efficiencies
*  A Seismic Shift in Application Security
*  Question: Industry Coffee Chat

### 3. Our Customers
*  Personas & Pain Points
*  VP of App Dev
*  DevOps Director
*  Head of IT
*  Chief Architect
*  Question: Personas & Pain Points
*  GitLab Digital Transformation CxO Discovery Guide
*  Group Activity: Call Planning & Preparation
*  Customer Success Stories & Proof Points

### 4. Our Portfolio
*  GitLab Pitch Deck
*  Question: Pitch Deck Video
*  Feature Comparison
*  Why Sell Ultimate/Gold?
*  Pricing
*  GitLab Direction
*  Product Maturity
*  Use Cases
*  Selling Professional Services

### 5. Sales Process
*  Overview: Command of the Message & MEDDPICC
*  Command of the Message Full E-Learning Course
*  MEDDPICC Full E-Learning Course
*  GitLab Value Framework
*  Question - Command of the Message eLearning 
*  Social Selling 101
*  Question: Business Development
*  Question: Account Development

### 6. Sales Action
*  Buyer Brief: Role Play Scenario
*  Role Play Preparation
*  1st Mock Call
*  Role Play Notes
*  Live Lead
*  MEDDPICC Opportunity Qualification
*  Group Activity: Positioning Value, Resolving Objections & Closing

### 7. Competitive Advantages & Strategy
*  Competitor Overview
*  GitLab vs. GitHub
*  Phone-A-Friend: Competitors
*  MSFT Azure DevOps + GitHub Competitive Card
*  CloudBees & Jenkins Competitive Card
*  Micro Focus Fortify Competitive Card
*  Question: Discovery & the Competition

### 8. Tools & Technology
*  GitLab Tech Stack Details
*  Update Your LinkedIn Profile!
*  DiscoverOrg 101 for Account Executives 
*  Outreach
*  Chorus.ai
*  You've Got Issues!
*  Clari for Salespeople
*  Salesforce - Booking Orders

### 9. Sales Support
*  Searching GitLab Like a Pro
*  Sales Handbook
*  Solution Architects
*  Customer Onboarding
*  Support for GitLab Team Members
*  GitLab Terms & Conditions
*  The GitLab Legal Team
*  Where to Find Sponsored Marketing Events

### 10. Course Evaluation & Feedback
*  Every participant is asked to please let us know how we can do better by taking [this brief survey](https://docs.google.com/forms/d/e/1FAIpQLScGYebV1pWMJLSw4mwHQ08k-w5Pn-s6GIp3bAU_xeBTfzDY5Q/viewform)


## **Sales Quickstart Workshop**
*  **Day 1** 
      - Welcome / Introductions / SQS Objectives
      - Breakout: Call Prep & Planning
      - Mock Discovery Call Exercise #1
      - Lunch
      - GitLab.com Basics Lab 1
         - Inspired by [No Tissues for Issues](https://bit.ly/GitLab-101) content
      - Mock Discovery Call Exercise #2
      - Breakout: Positioning Value, Resolving Objections & Closing
      - Day 1 Recap
      - Group Dinner
*  **Day 2**
      - Day 2 Outlook
      - Mock Discovery Call Exercise #3
      - Discussion: Product Tiering
      - Breakout: Territory / Account Planning
      - Lunch
      - Breakout: Territory / Account Planning (continued)
      - Mock Discovery Call Exercise #4
      - GitLab.com Basics Lab 2: Submit Your Own MR
      - GitLab Handbook Scavenger Hunt
      - Day 2 Recap
*  **Day 3**
      - Day 3 Outlook
      - Discussion: GitLab Alliances
      - Discussion: [GitLab Customer Use Cases](https://about.gitlab.com/handbook/use-cases/)
      - Discussion: Customer Stories
      - Discussion: Competitors
      - Lunch with Q&A
      - Feedback & Wrap-up
